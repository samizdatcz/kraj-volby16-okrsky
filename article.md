---
title: "Jak volili vaši sousedi? Prohlédněte si nejpodrobnější mapu volebních výsledků"
perex: "Bližší pohled na výsledky letošních krajských voleb může překvapit. V Královéhradeckém kraji, kde přesvědčivě zvítězilo ANO, leží obec Jívka, v níž 46 procent hlasů patřilo komunistům. Ve Zlínském kraji, který se z volebních výsledků jeví jako bašta lidovců, zase ve Vlčnově získala více než 40procentní podporu ODS. Z podrobné mapy Českého rozhlasu teď můžete zjistit, které obce se nejvíce vzepřely celkovým výsledkům, kde mají jednotlivé strany největší podporu i jak volili vaši sousedé."
description: "Prozkoumejte nejpodrobnější volební výsledky všech stran."
authors: ["Jan Cibulka", "Michal Zlatkovský", "Petr Kočí"]
published: "11. října 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "krajske-volby-v-okrscich"
socialimg: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/media/socimg.jpg
libraries: [jquery, "https://api.tiles.mapbox.com/mapbox-gl-js/v0.25.1/mapbox-gl.js", "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.js", "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"]
styles: ["https://api.tiles.mapbox.com/mapbox-gl-js/v0.25.1/mapbox-gl.css", "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.css", "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/senatni-volby-v-okrscich/
    title: "Podívejte se, kdo zvolil vašeho senátora. Přinášíme výsledky druhého kola okrsek po okrsku"
    perex: "Interaktivní mapa, kam Český rozhlas zakreslil výsledek druhého kola senátních voleb v každém jednotlivém okrsku. Prozkoumejte ji a zjistíte, jak a kde uspěli všichni kandidáti do Senátu."
    image: https://interaktivni.rozhlas.cz/data/senat-volby-okrsky/www/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/zisky-v-krajskych-volbach/
    title: "Mapa: ČSSD ztrácela napříč republikou, ANO triumfuje"
    perex: "Srovnání zisků jednotlivých stran v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011 Andreje Babiše."
    image: https://interaktivni.rozhlas.cz/data/kraj-volby-zisky/www/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/
    title: Doleva, či doprava. Podívejte se, jak volí vaše obec
    perex: Český rozhlas připravil aplikaci, kde zjistíte, čím je vaše obec v kraji výjimečná: Které strany zde vedou, a které naopak propadají.
    image: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/media/socimg.jpg
---

## Tato mapa se váže ke krajským volbám 2016, <a href="https://www.irozhlas.cz/volby/jak-volili-vasi-sousedi-prohlednete-si-nejpodrobnejsi-mapu-volebnich-vysledku_1710220940_pek">aktuální mapu k volbám do sněmovny 2017 najdete zde</a>.

### Prohlédněte si také [mapu výsledků senátních voleb v každém okrsku](https://interaktivni.rozhlas.cz/senatni-volby-v-okrscich/)

<aside class="big">
<div id="mapa">
  <div id='selector'>
    <select class="selectVals"></select>
  </div>
  <div id='info'>Najetím na mapu vyberte okrsek.</div>
  <div id='map'></div>
</div>
</aside>

_Zdroj: [Volby.cz](http://volby.cz)_

Pokud se výsledky stran v některých krajích nezobrazují, kandidovaly v nich pravděpodobně jako součást koalic.